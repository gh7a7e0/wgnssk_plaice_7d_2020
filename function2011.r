###################! Functions used for the explonatory analysis in 2010
##### R.14.2
#####



### totalStk
## Add up totals for stock objects, computes landings, discards, catch, stock totals
totalStk <- function(stk){
    landings(stk) <- computeLandings(stk)
    discards(stk) <- computeDiscards(stk)
    ## no discard yet
    catch.n(stk)  <- landings.n(stk)+discards.n(stk)
    catch.wt(stk) <-(landings.n(stk)*landings.wt(stk)+discards.n(stk)*discards.wt(stk))/catch.n(stk)
    catch(stk)    <- computeCatch(stk)
    stock(stk)    <- computeStock(stk)
    units(stk)[1:17] <- as.list(c(rep(c("tonnes","thousands","kg"),4), "NA", "NA", "f", "NA", "NA"))
    return(stk)
    }


write.wg.txt.stock <- function(number, x, slot, path=result.path, round=0){
  capture.output(cat("Table", number," ", x@name,":", slot, "\n", as.character(Sys.time()), " units=",
    units(slot(x,slot)), "\n"), file=paste(path,"/","Table", number," ",slot,".txt", sep=""))
  capture.output(matrix(round(slot(x, slot),round), dims(slot(x,slot))$year, byrow=T,dimnames=list(year =
    dims(slot(x, slot))$minyear:dims(slot(x, slot))$maxyear, age=dims(slot(x, slot))$min:dims(slot(x,
    slot))$max)), file=paste(path,"/","Table", number," ",slot,".txt", sep=""), append=T)
}

write.wg.txt.indices <- function(number, x, name, filename, path=result.path, round=0){
capture.output(cat("table", number, ".", name, "\n", as.character(Sys.time()), file=paste(path,"/",filename, sep="")))
for (i in 1:length(x)){
    capture.output(print(paste(x[[i]]@name," units=", units(x[[i]]@index)),quote=F), file=paste(path,"/",filename, sep=""),append=T)
    capture.output(print(cbind(x[[i]]@effort,matrix(x[[i]]@index,dims(x[[i]]@index)$year,byrow=T, dimnames=list(year=dims(x[[i]]@index)$minyear:dims(x[[i]]@index)$maxyear, age=dims(x[[i]]@index)$min:dims(x[[i]]@index)$max))), width=6, digits=3), file=paste(path,"/",filename, sep=""), append=T)
    }
}

stock.summary <- function(object, rage=1, fbar.min=2, fbar.max=6, type="missing"){
  if (!inherits(object, "FLStock")) stop("Object must be an 'FLStock' object!")
   d.f <- NULL
      year.  <- dimnames(object@stock.n)[[2]]
      r.     <- as.data.frame(round(object@stock.n[as.character(rage),,,,],0))["data"]
      ssb.   <- as.data.frame(round(ssb(object),0))["data"]
      f.     <- as.data.frame(round(apply(slot(object, "harvest")[fbar.min:fbar.max,], 2:5, mean), 3))["data"]
      c.     <- as.data.frame(round(object@catch,0))["data"]
      l.     <- as.data.frame(round(object@landings,0))["data"]
      d.     <- as.data.frame(round(quantSums(object@stock.n*object@stock.wt),0))[,"data"]
      ydssb. <- as.data.frame(round(object@landings/ssb(object),2))["data"]
      res  <- rbind(d.f, cbind(r., ssb., c., l., d., f., ydssb.))
      colnames(res) <- c("recruitment","ssb","catch","landings","tsb",paste("fbar",fbar.min,"-",fbar.max,sep=""),"Y/ssb")
      rownames(res) <- year.
     return(res)
 }
 #---------------------------------------------------

SingleRun <- function(My.Stock, My.Tuning.Fleets, My.Control, Short.name) {
  My.single <- My.ssb.1 <- list()
  for (i in 1:length(My.Tuning.Fleets)) {
    My.idx.1 <- FLIndices(My.Tuning.Fleets[[i]])
    names(My.idx.1) <- Short.name[i]
    My.xsa.1 <- FLXSA(My.Stock, My.idx.1, My.Control)
    My.xsa.1@index.name <- Short.name[i]
    #My.sel.1 <- transform(My.Stock, stock.n = My.xsa.1@stock.n, harvest=My.xsa.1@harvest)
    #My.ssb.1[[i]] <- ssb(My.sel.1)

    My.single[[i]] <- My.xsa.1
    }
return(My.single)
}
#---------------------------------------------------
# q residuals plot
qresid.plot <- function(My.xsa, yr) {
if (missing(yr)) yr <- range(My.xsa@index.res)

for (i in 1:length(My.xsa@index.name)) {
  if (i == 5) {
    windows()
    par(mfrow=c(2,2))
  }
  n.age <- as.numeric(dimnames(My.xsa@index.res[[i]])$age)
   plot(dimnames(My.xsa@index.res[[i]])$year,My.xsa@index.res[[i]][1,],type="l",pch=n.age[1],ylim=yr,ylab="q residuals",main=My.xsa@index.name[i],xlab="")
   for (j in 1:dim(My.xsa@index.res[[i]])[1])   {
    lines(dimnames(My.xsa@index.res[[i]])$year,My.xsa@index.res[[i]][j,])
    points(dimnames(My.xsa@index.res[[i]])$year,My.xsa@index.res[[i]][j,],pch=n.age[j],cex=0.9)
    }
   abline(h=0,lty=2)
   legend(as.numeric(dimnames(My.xsa@index.res[[i]])$year[1]),yr[2],as.character(1:9),pch=1:9,lty=1,horiz=TRUE,cex=0.6)
 }
abline(h=0,lty=2)

}
#---------------------------------------------------

createSingle <- function(MyStock, My.idx, My.xsa.control) {

  My.single <- My.ssb.1 <- list()

  for (i in 1:length(My.idx@names)) {
      My.idx.1 <- FLIndices(My.idx[[i]])
      names(My.idx.1) <- My.idx@names[i]

      My.xsa.1 <- FLXSA(MyStock, My.idx.1, My.xsa.control)
      My.sel.1 <- transform(MyStock, stock.n = My.xsa.1@stock.n, harvest=My.xsa.1@harvest)
      My.ssb.1[[i]] <- ssb(My.sel.1)

      My.single[[i]] <- My.xsa.1
    }
return(list(SR = My.single, SSB.SR = My.ssb.1))
}
#---------------------------------------------------
CreateGraphSR <- function(My.ssb.1, My.ssb, My.Single, My.Fbar, Fmin, Fmax, FleetNames,CEX=0.75, shift.y.ssb = c(0,0,0,0,0), shift.y.F = c(0,0,0,0,0))  {
  # edited by CPL so works with differing numbers of fleets (was hardwired for i up to 7)
  LTY.lines <- 1:length(My.ssb.1);#c(1,1,1,1,2,2,1)
  COL.lines <- c('light blue','green','purple','dark grey','black','turquoise','orange','dark blue','green','red','dark grey','black','pink','purple','blue','green','orange','grey','black','pink','purple');
  xr <- as.numeric(dimnames(My.ssb.1[[1]])$year)
  y.text <- unlist(My.ssb.1)[seq (length(xr),  length(unlist(My.ssb.1)), by = length(xr))]

  MAXSINGLE<-max(My.ssb)
  for (i in 1:length(My.ssb.1)) MAXSINGLE<-max(MAXSINGLE,as.numeric(My.ssb.1[[i]]))

  plot(xr, c(My.ssb), type="l", ylim = c(0,MAXSINGLE), lty = 1, ylab = "SSB", xlab = "", col="red", lwd=2)
    for (i in 1:length(My.ssb.1)) lines(xr, as.numeric(My.ssb.1[[i]]), lty = LTY.lines[i],col=COL.lines[i])
    legend('topleft', c("Final run", FleetNames),lty=c(1,LTY.lines),lwd=c(2,rep(1,length(COL.lines))),col=c(2,COL.lines),cex=CEX)

  last.F <- numeric(length(My.ssb.1)); Fyr<-2006
  for (i in 1:length(My.ssb.1)) last.F[i] <- apply(My.Single[[i]]@harvest[Fmin:Fmax,],2,mean)[,paste(Fyr)]
  f3.6 <- apply(My.Single[[1]]@harvest[Fmin:Fmax,],2,mean)
  Fbar.lab <- paste("Fbar(",Fmin,"-",Fmax,")",sep="")
                                         #min(range(last.F)[1]/2,range(My.Fbar)[1],0.4)     max(range(My.Fbar)[2],1)
  plot(xr, c(My.Fbar), type="l", ylim = c(0,1.1), lty = 1, ylab = Fbar.lab, xlab = "year", col="red", lwd=2)
    for (i in 1:(length(My.ssb.1))) lines(xr, apply(My.Single[[i]]@harvest[Fmin:Fmax,],2,mean) , lty = LTY.lines[i],col=COL.lines[i])
    LEG<-NULL; for (i in 1:(length(My.ssb.1))) LEG <- cbind(LEG, paste( FleetNames[i],'F',round(last.F[i],3) ) )
    legend('topleft', c("Final run", LEG),lty=c(1,LTY.lines),lwd=c(2,rep(1,length(COL.lines))),col=c(2,COL.lines),cex=CEX)
    abline(v=Fyr,lty=3,col='grey')
}


fleetbubbles<-function(My.xsa,firstyear,lastyear,result.path){
  #-fleet residual plots-
  windows(width=5.9,height=7.8)
  par(mfrow=c(length(My.xsa@index),2))
  #-find largest residual for scaling-
  rmax <- 0
  for(i in 1:length(My.xsa@index)){
     rmax <- max(abs(na.omit(My.xsa@index.res[[i]])),rmax,na.rm=T)
  }

  for(i in 1:length(My.xsa@index)){
     years<-firstyear:lastyear#seq(MyStock@range[4:5,2])
     temp <- as.data.frame(My.xsa@index.res[[i]])
     scale <- 7/max(temp$data,na.rm=T)
     #---bubble plots---
     par(mar=c(3,5,1,3))
     plot(years,rep(0,length(years)),pch=" ",ylim=c(min(temp$age)-1,max(temp$age)+1),
        xlim=c(min(temp$year)-1,max(temp$year)+1),ylab=paste(My.xsa@index.name[[i]], sep=""))
     points(temp$year, temp$age, pch=ifelse(is.na(temp$data),79,21)
     , cex=ifelse(is.na(temp$data),2,scale * abs(temp$data)),
            bg=ifelse(is.na(temp$data),"green",ifelse(temp$data > 0,"red","blue")))
     #---line plots---
     par(mar=c(3,1,1,3))
     plot(years,rep(0,length(years)),pch=" ",xlim=c(min(temp$year)-1,max(temp$year)+1),
        ylim=c(-rmax,rmax))
     lines(((min(temp$year)-1):(max(temp$year)+1)),rep(0,length(min(temp$year):max(temp$year))+2))
     for (a in min(temp$age):max(temp$age)){
        lines(temp$year[temp$age==a], temp$data[temp$age==a],lty=a-min(temp$age)+1,col=sort(rep(c("black","grey"),6))[a-min(temp$age)+1],
              ylab=NULL)
     }
     legend( "bottomleft",
             legend=paste(min(temp$age):max(temp$age),sep=""),
             lty=(min(temp$age):max(temp$age)- min(temp$age))+1,
             col=sort(rep(c("black","grey"),6)),cex=0.5,horiz=TRUE)
  }
  savePlot(filename = paste(result.path,'fleetresiduals ',sep=""), type = 'png')
  #if(is.element(paste(output.path,fleetresiduals,".",plot.type,sep=""),plots)==FALSE){
  #  plots <- append(plots, paste(output.path,fleetresiduals,".",plot.type,sep=""),after=length(plots))
  #}
}




diagnosticsold <-  function(object){

    indices<-new("FLIndices")
    for (i in 1:length(object@index))
        {
        indices[[i]] <- FLIndex(index=object@index[[i]])
        indices[[i]]@name  <-object@index.name[i]
        indices[[i]]@range <-object@index.range[[i]]
        }
    control <-object@control #<- eval(parse(text=xsa@call[4]))

    cat("FLR XSA Diagnostics ",  as.character(Sys.time()),"\n\nCPUE data from ", object@call[3], "\n\n",
        "Catch data for ", dims(object@stock.n)$year," years. ", dims(object@stock.n)$minyear," to ",
        dims(object@stock.n)$maxyear, ". Ages ",dims(object@stock.n)$min," to ",dims(object@stock.n)$max,".\n\n", sep="")

    # print general tuning series information
    idx.info  <- NULL
    for (i in 1:length(object@index)) {
       idx.info  <-  rbind(idx.info,c(indices[[i]]@name, (dims(object@index[[i]]))$min,
          (dims(object@index[[i]]))$max,(dims(object@index[[i]]))$minyear,(dims(object@index[[i]]))$maxyear,
          indices[[i]]@range["startf"], indices[[i]]@range["endf"]))
    }

    dimnames(idx.info) <- list(NULL,c("fleet","first age","last age","first year","last year","alpha","beta"))
    print(as.data.frame(idx.info))
    cat("\n\n","Time series weights :\n\n")
    cat(ifelse(control@tsrange==0|control@tspower==0, "   Tapered time weighting not applied\n\n",
        paste("   Tapered time weighting applied\n", "  Power =  ",control@tspower,"over ",control@tsrange,
        "years\n\n", sep=" ")))

    cat("Catchability analysis :\n\n")
    cat(ifelse(as.numeric(control@rage) < dims(object@stock.n)$min, "    Catchability independent of size for all ages\n\n",
        paste("    Catchability independent of size for ages >  ",control@rage,"\n\n",sep=" ")))
    cat(ifelse(as.numeric(control@qage) < dims(object@stock.n)$min, "    Catchability independent of age for all ages\n\n",
        paste("    Catchability independent of age for ages >  ",control@qage,"\n\n",sep=" ")))

    cat("Terminal population estimation :\n\n")
    cat(ifelse(control@shk.f, paste("    Survivor estimates shrunk towards the mean F\n",
        "   of the final  ",control@shk.yrs,"years or the ",control@shk.ages,"oldest ages.\n\n",
        "   S.E. of the mean to which the estimates are shrunk =  ", control@fse,"\n",sep=" "),
        "    Final estimates not shrunk towards mean F\n"))
    cat(ifelse(as.numeric(control@min.nse)==0, "\n", paste("\n", "   Minimum standard error for population\n",
        "   estimates derived from each fleet = ",control@min.nse,"\n\n", sep=" ")))
    cat("   prior weighting not applied\n\n")

    cat("Regression weights\n")
    ### Calculation of time series weighting
    yr.range <- max(dims(object@harvest)$minyear, dims(object@harvest)$maxyear-9):dims(object@harvest)$maxyear
    regWt <- FLQuant(dimnames=list(age = 'all', year = yr.range))
    for(y in yr.range) regWt[,as.character(y)] <- (1-((max(yr.range)-y)/control@tsrange)^control@tspower)^control@tspower
    print(matrix(round(regWt,3),dims(regWt)$age,dimnames=list(age="all",year=yr.range)))

    cat("\n\n Fishing mortalities\n")
    print(matrix(round(trim(object@harvest,year=yr.range),3), dims(object@harvest)$age,
        dimnames=list(age=dims(object@harvest)$min:dims(object@harvest)$max, year=yr.range)))

    cat("\n\n XSA population number (",object@stock.n@units,")\n")
    print(t(matrix(round(trim(object@stock.n,year=yr.range),0), dims(object@stock.n)$age,
        dimnames=list(age=dims(object@stock.n)$min:dims(object@stock.n)$max, year=yr.range))))

    nextyear  <- dims(object@survivors)$maxyear
    cat("\n\n Estimated population abundance at 1st Jan ",nextyear,"\n")
    print(t(matrix(round(object@survivors[,as.character(nextyear)]),
        dimnames=list(age=dims(object@survivors)$min:dims(object@survivors)$max, year=nextyear))))

    ## tuning info
    for (f in 1:length(object@index)) {
        cat("\n\n Fleet: ",indices[[f]]@name,"\n\n","Log catchability residuals.\n\n")

        print(matrix(round(object@index.res[[f]],3), nrow=dims(object@index.res[[f]])$age,
            dimnames=list(age=dimnames(object@index.res[[f]])$age, year=dimnames(object@index.res[[f]])$year)))

        if (control@rage < dims(object@index[[f]])$max){
          cat("\n\n Mean log catchability and standard error of ages with catchability \n",
              "independent of year class strength and constant w.r.t. time \n\n")

          q.tab <- rbind(Mean_Logq=round(log(object@q.hat[[f]]),4), S.E_Logq=round(sd(matrix(object@index.res[[f]],
              dim(object@index.res[[f]])[2],dim(object@index.res[[f]])[1],byrow=T),na.rm=T),4))
          colnames(q.tab) <- dimnames(object@q.hat[[f]])$age

          if (dims(object@index[[f]])$min <= control@rage ) {
              print(q.tab[,as.character((control@rage+1):max(as.numeric(dimnames(object@index[[f]])$age)))])
          } else {print(q.tab)}
        }
        # print reg stats powermodel, note that maximum printed age is min(rage, max(age in tun series))
        if (dims(object@index[[f]])$min <= control@rage ) {
            cat("\n Regression statistics \n", "Ages with q dependent on year class strength \n")
            print(cbind((matrix(object@q2.hat[[f]][as.character(dims(object@index[[f]])$min:(min(control@rage,dims(object@index[[f]])$max)))],dimnames=list(age=paste("Age ",dims(object@index[[f]])$min:(min(control@rage,dims(object@index[[f]])$max)),sep=""),"slope"))),
            (matrix(object@q.hat[[f]][as.character(dims(object@index[[f]])$min:(min(control@rage,dims(object@index[[f]])$max)))],dimnames=list(age=dims(object@index[[f]])$min:(min(control@rage,dims(object@index[[f]])$max)),"intercept")))))
        }
    }
 cat("\n\n Terminal year survivor and F summaries: \n ")
    for ( a in sort(unique(object@diagnostics$age))){
        yrcl  <-  max(object@diagnostics$year) - a
        cat("\n Age ",a, " Year class = ",  yrcl ," \n\n","source \n", sep="")
        wts <- subset(object@diagnostics, age<=a & yrcls== yrcl)
        # calc surivors and scaled wts
        N <- table(wts$source)
        wts <- cbind(wts,prop.wts=prop.table(wts$w))
        scaledWts <- round(tapply(wts$prop.wts,wts["source"],sum),3)
        wts <- cbind(wts,wtd.nhat=(wts$w*wts$nhat))
        survivors <- round(exp(tapply(wts$wtd.nhat,wts["source"],sum)/tapply(wts$w,wts["source"],sum)))
        weights <- cbind(survivors, N, scaledWts)
        n.ind <- unlist(lapply(indices, name))[unlist(lapply(indices, name)) %in% unique(wts$source)]
        shk   <- unique(wts$source)[!(unique(wts$source)%in% n.ind)]
        print(weights[c(n.ind,shk),])
    }
    invisible()
}

## ===========================================================
## Making an input table (thanks to Espen Johnson)
inputTableSTF <- function(object){
  #note: for reasons of convenience I have removed calculation and output of fdisc and fland
  f.  <- as.data.frame(object@harvest)
  n.  <- as.data.frame(object@stock.n)[,"data"]
  cw. <- as.data.frame(object@catch.wt)[,"data"]
  lw. <- as.data.frame(object@landings.wt)[,"data"]
  dw. <- as.data.frame(object@discards.wt)[,"data"]
  mat.<- as.data.frame(object@mat)[,"data"]
  m.  <- as.data.frame(object@m)[,"data"]
  d.f <- cbind(f., n., cw., lw., dw., mat., m.)[,c(-3,-4,-5,-6)]
  colnames(d.f) <- c("age",	"year",	"f", "stock.n"	, "catch.wt", "landings.wt", "discards.wt", "mat", "M")
  return(d.f)
}

## ===========================================================
## Making a management option table (thanks to Espen Johnson)
## -----------------------------------------------------------
## First we make a function that takes a vector of f multipliers 
## that we want to present the results for in the table. This 
## function returns a list of FLSTF objects, from which we only
## keep the forecasted years. 
scanSTF <- function(stock, FLSTF.control="missing", fscan=c(0.8, 1.0, 1.2), survivors=NA){
    if (missing(FLSTF.control)) stop("no FLSTF.control") 
    if (!inherits(stock, "FLStock") ) stop("stock must be 'FLStock' object")
    res <- list()
    for (it in fscan) {
       cat("Fmult: ", it, "\n")
       if (length(FLSTF.control@fmult) == 1) FLSTF.control@fmult <- it
       else FLSTF.control@fmult[2:length(FLSTF.control@fmult)] <-  it
       tmp <- FLSTF(stock, FLSTF.control, survivors=survivors)
       res[[as.character(it)]] <- window(tmp, start=stock@range["maxyear"]+1, end=tmp@range["maxyear"])
    }
    return(res)
}


summTableSTF  <- function(object){
    d.f <- NULL
    for (i in 1:length(object) ) {
        fmult <- object[[i]]@control@fmult[2]
        fbar.min <- object[[i]]@control@fbar.min
        fbar.max <- object[[i]]@control@fbar.max
        ssb. <- as.data.frame(round(ssb(object[[i]]),0))
        f.   <- as.matrix(c(round(apply(slot(object[[i]], "harvest")[as.character(fbar.min:fbar.max),,,,], 2:5, mean),3)))
        r.   <- as.matrix(c(round(object[[i]]@stock.n[1,],0)))
        c.   <- as.matrix(c(round(object[[i]]@catch,0)))    
        f.d  <- as.matrix(c(round(apply((slot(object[[i]], "harvest") * object[[i]]@discards.n/object[[i]]@catch.n)[as.character(2:3),], 2:5, mean),2)))
        f.l  <- as.matrix(c(round(apply(slot(object[[i]], "harvest")[as.character(fbar.min:fbar.max),,,,], 2:5, mean),2)))
        l.   <- as.matrix(c(round(apply(object[[i]]@landings.n*object[[i]]@landings.wt,2,sum, na.rm=TRUE),0)))
        d.   <- as.matrix(c(round(apply(object[[i]]@discards.n*object[[i]]@discards.wt,2,sum, na.rm=TRUE),0)))
        d.f  <- rbind(d.f, cbind(fmult, ssb., f., f.d, f.l, r., c., l., d.))
    }
    colnames(d.f) <- c("fmult", "age","year","unit","season","area","iter","ssb",
    paste("f",fbar.min,"-",fbar.max,sep=""),"f_dis2-3", "f_hc2-6","recruit", "catch", "landings", "discards")
    return(d.f)
}
